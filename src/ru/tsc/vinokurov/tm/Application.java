package ru.tsc.vinokurov.tm;

public final class Application {

    private static void displayHelp() {
        System.out.printf("%s - Display program version.\n", TerminalConst.CMD_VERSION);
        System.out.printf("%s - Display developer info.\n", TerminalConst.CMD_ABOUT);
        System.out.printf("%s - Display list of terminal commands.\n", TerminalConst.CMD_HELP);
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Developer: Aleksey Vinokurov");
        System.out.println("Email: avv96@yandex.ru");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("Version: 1.2.0");
        System.exit(0);
    }

    private static void displayError(String arg) {
        System.err.printf("Error! %s is not valid argument!\n", arg);
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void processArgs(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            default:
                displayError(arg);
        }
    }

    public static void main(String[] args) {
        displayWelcome();
        processArgs(args);
    }

}
